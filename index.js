/*
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)

- це об'єктна модель документа, містить всі сторінки як об'єкти, які можна змінювати через js файл.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

- різниця проста, при  innerHTML текст елементу і самі теги будуть повертатись.
А при використанні innerText, буде повернуто сам текст без тегів
Наприклад:

<div id="mainHeader">
    <h1>Lalala</h1>
</div>

const headerText = document.querySelector('#mainHeader');
console.log(some.innerText); // Lalala
console.log(some.innerHTML); // <h1>Lalala</h1>


3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
 Звернутись можна тикими способами:
- querySelector()
- querySelectorAll()
- getElementById()
- getElementsByClassName()
- getElementsByName()
- getElementsByTagName()

Найкращий спосіб це querySelector() - тому що використовуються CSS селектори для пошуку елемента, є простішим взагалі.

4. Яка різниця між nodeList та HTMLCollection?

- NodeList має властивості псевдомасиву і щодо нього можна використати методи forEach() і т.і. або використати властивість length, а HTMLCollection не має таких методів.
На відміну від HTMLCollection в деяких випадках NodeList є статичною колекцією. Це означає, що будь-які зміни в DOM не позначаються на його змісті. 
Наприклад, document.querySelectorAll повертає статичний NodeList.

Практичні завдання
1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
Використайте 2 способи для пошуку елементів.
Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
*/

const findFeatureByClassName = document.getElementsByClassName('feature');
console.log(findFeatureByClassName);

const findFeatureByQuery = document.querySelectorAll('.feature');

console.log(findFeatureByQuery);

findFeatureByQuery.forEach((feature) => {
    feature.style.textAlign = 'center';
});

/*
2. Змініть текст усіх елементів h2 на "Awesome feature".
*/

const headerTwoText = document.querySelectorAll('h2');

headerTwoText.forEach((elem) => {
    elem.textContent = 'Awesome feature';
});

/*
3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
*/

const addExclamation = document.querySelectorAll('.feature-title');

addExclamation.forEach((elem) => {
    const text = elem.textContent;
    elem.textContent = text + '!';
});
console.log(addExclamation)

